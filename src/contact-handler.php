<?php

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

$raw = file_get_contents("php://input");

$data = json_decode($raw);

$pass = getenv('MARIA_EMAIL_PASS'); 

$adminmail = 'mariia.kaptur@gmail.com';

$email_from = 'contact@mariakaptur.com';
$email_subject = 'Nouvelle saisie du formulaire - mariakaptur.com';
$email_body =   "Prénom: " . $data->firstname ."\n" .
                "Nom : " . $data->lastname . "\n" .
                "Email: " . $data->email . "\n" .
                "Phone: " . $data->phone . "\n" .
                "Message : " . $data->message . "\n";

$to = 'mariia.kaptur@gmail.com';


//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = 'mariakaptur.graph@gmail.com';
//Password to use for SMTP authentication
$mail->Password = $pass;
//Set who the message is to be sent from
$mail->setFrom($data->email, $data->firstname . ' ' . $data->lastname);
//Set who the message is to be sent to
$mail->addAddress($adminmail, 'admin');
//Set the subject line
$mail->Subject = $email_subject;

//Replace the plain text body with one created manually
$mail->Body = $email_body;

//send the message, check for errors
if (!$mail->send()) {
    http_response_code(500);
    var_dump( "Mailer Error: " . $mail->ErrorInfo);
} else {
    http_response_code(200);
    var_dump("Message sent!");
    //Section 2: IMAP
    //Uncomment these to save your message in the 'Sent Mail' folder.
    #if (save_mail($mail)) {
    #    echo "Message saved!";
    #}
}

?>
