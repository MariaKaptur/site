'use strict'

//NAVBAR RESIZE
var navbar = document.querySelector(".navbar"),
    logo = document.querySelector("#logo"),
    navbarBrand = document.querySelector(".navbar-brand"),
    phoneNumber = document.querySelector(".phone-number"),
    bgDark = document.querySelector(".bg-dark");

window.addEventListener('scroll', function (e) {
    var y = window.pageYOffset;
    if (y > 91) {
        navbar.style.paddingBottom = 0.5 + "rem";
        navbar.style.paddingTop = 0.5 + "rem";
        logo.style.height = 45 + "px";
        logo.style.width = 45 + "px";
        navbarBrand.style.fontSize = 1.4 + "rem";
        phoneNumber.style.backgroundColor = "#fff";
        phoneNumber.style.color = "#f92a82";
        bgDark.style.opacity = 0.85;
    } else {
        navbar.style.paddingTop = 1.5 + "rem";
        navbar.style.paddingBottom = 1.5 + "rem";
        logo.style.height = 50 + "px";
        logo.style.width = 50 + "px";
        navbarBrand.style.fontSize = 1.6 + "rem";
        phoneNumber.style.backgroundColor = "#f92a82";
        phoneNumber.style.color = "#fff";
        bgDark.style.opacity = 1;
    }
});

//DYNAMIC GALLERY
console.log('JS EN loadedz');
let modal;
let btn;
let span;
let index;
let imgPrint =

    [


        {
            href: '../img/portfolio/print/print (3).png',
            data: 'portfolio',
            legend: 'Flyer design: Cover'
        },

        {
            href: '../img/portfolio/print/print (2).png',
            data: 'portfolio',
            legend: 'Flyer design: Interior'
        },

        {
            href: '../img/portfolio/print/print (4).png',
            data: 'portfolio',
            legend: 'A4 Flyer: Cover'
        },

        {
            href: '../img/portfolio/print/print (5).png',
            data: 'portfolio',
            legend: 'A4 Flyer: Interior'
        },

        {
            href: '../img/portfolio/print/print (7).png',
            data: 'portfolio',
            legend: 'Business card 1'
        },

        {
            href: '../img/portfolio/print/print (9).png',
            data: 'portfolio',
            legend: 'Business card 2'
        },

        {
            href: '../img/portfolio/print/print (10).png',
            data: 'portfolio',
            legend: 'A3 poster'
        },

    ];

let imgInte =

    [

        {
            href: '../img/portfolio/inte/intePerso.png',
            data: 'portfolio',
            legend: 'Portfolio layout, desktop version. Layout created with Photoshop and implemented with HTML and CSS (Bootstrap)'
        },

        {
            href: '../img/portfolio/inte/intePersoMobile.png',
            data: 'portfolio',
            legend: 'Portfolio layout, mobile version'
        },

        {
            href: '../img/portfolio/inte/inteTea1.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteTea2.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteTea3.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteTea3mobile.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteMind1.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteMind1mobile.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteMind2.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

        {
            href: '../img/portfolio/inte/inteMind2mobile.png',
            data: 'portfolio',
            legend: 'Integration of a commercial website'
        },

    ];

let imgLogo =

    [

        {
            href: '../img/portfolio/logo/logo (1).png',
            data: 'portfolio',
            legend: 'Logo example 1'
        },

        {
            href: '../img/portfolio/logo/logo (2).png',
            data: 'portfolio',
            legend: 'Logo example 2'
        },

        {
            href: '../img/portfolio/logo/logo (3).png',
            data: 'portfolio',
            legend: 'Logo example 3'
        },

        {
            href: '../img/portfolio/logo/logo (4).png',
            data: 'portfolio',
            legend: 'Logo example 4'
        },

        {
            href: '../img/portfolio/logo/logo (5).png',
            data: 'portfolio',
            legend: 'Logo example 5'
        },

        {
            href: '../img/portfolio/logo/logo (6).png',
            data: 'portfolio',
            legend: 'Logo example 6'
        },

        /*{
            href: '../img/portfolio/others/mobile-device.png',
            data: 'portfolio',
            legend: 'Jeu mobile <a href="https://play.google.com/store/apps/details?id=com.pwnedstudios.idlepizzaclicker&hl=fr_FR" target="_blank">"IDLE Pizza Clicker"</a> que j\'ai illustré (Pwned Studios)'
        },

        {
            href: '../img/portfolio/others/pizza1.png',
            data: 'portfolio',
            legend: 'Exemples de pizzas standard'
        },

        {
            href: '../img/portfolio/others/pizza2.png',
            data: 'portfolio',
            legend: 'Exemples de pizzas prémium'
        },

        {
            href: '../img/portfolio/others/fours.png',
            data: 'portfolio',
            legend: 'Exemples de fours'
        },*/

        {
            href: '../img/portfolio/others/mix-watercolor.png',
            data: 'portfolio',
            legend: 'Watercolor illustration example'
        },

        {
            href: '../img/portfolio/others/watercolor-interieur.png',
            data: 'portfolio',
            legend: 'Watercolor illustration example'
        },

        {
            href: '../img/portfolio/others/mermaids-fontain.png',
            data: 'portfolio',
            legend: 'Watercolor illustration example + photo montage'
        },

        {
            href: '../img/portfolio/others/youtube.png',
            data: 'portfolio',
            legend: '<a href="https://www.youtube.com/channel/UCnwYlXGPPPRz06kvC73fNDw?view_as=subscriber" target="_blank">Some videos I created</a> using After Effects (motion graphics, motion text) and HitFilm Express (Adobe Premiere equivalent)'
        },

        {
            href: '../img/portfolio/others/hello.gif',
            data: 'portfolio',
            legend: 'Animation created with After Effects'
        },

        /*{
            href: '../img/portfolio/others/perso-gif.gif',
            data: 'portfolio',
            legend: 'Walk cycle'
        },*/

        /*{
            href: '../img/portfolio/others/banner-site.gif',
            data: 'portfolio',
            legend: 'Bannière animée pour un site'
        },*/

    ];

let imgDigitart =

    [

        {
            href: '../img/portfolio/digitart/digitart (1).png',
            data: 'portfolio',
            legend: 'Digital illustration example'
        },

        {
            href: '../img/portfolio/digitart/digitart (4).png',
            data: 'portfolio',
            legend: 'Digital illustration example'
        },

        /*{
            href: '../img/portfolio/digitart/digitart (2).png',
            data: 'portfolio',
            legend: 'Peinture numérique'
        },*/

        {
            href: '../img/portfolio/digitart/digitart (3).png',
            data: 'portfolio',
            legend: 'Digital illustration example'
        },

        {
            href: '../img/portfolio/digitart/digitart (5).png',
            data: 'portfolio',
            legend: 'Digital illustration for corporate social media'
        },

        {
            href: '../img/portfolio/digitart/digitart (6).png',
            data: 'portfolio',
            legend: 'Digital illustration for corporate social media'
        },

        {
            href: '../img/portfolio/digitart/digitart (7).png',
            data: 'portfolio',
            legend: 'Digital illustration for corporate social media'
        },

        {
            href: '../img/portfolio/digitart/digitart (8).png',
            data: 'portfolio',
            legend: 'Digital illustration for corporate social media'
        },

        /*{
            href: '../img/portfolio/digitart/digitart (9).png',
            data: 'portfolio',
            legend: 'Illustrations pour les réseaux sociaux'
        },*/

        {
            href: '../img/portfolio/digitart/digitart (10).png',
            data: 'portfolio',
            legend: 'Digital painting: Occitanie, France'
        },

        {
            href: '../img/portfolio/digitart/digitart (11).png',
            data: 'portfolio',
            legend: 'Digital painting: A cat in Paris'
        },

        {
            href: '../img/portfolio/digitart/digitart (12).png',
            data: 'portfolio',
            legend: 'Pixel art: Birthday card'
        },

        {
            href: '../img/portfolio/digitart/digitart (13).png',
            data: 'portfolio',
            legend: 'Digital painting: book cover'
        },

        /*{
            href: '../img/portfolio/digitart/digitart (14).png',
            data: 'portfolio',
            legend: 'Digital illustration for roller association social media'
        },*/

    ];

/*var modalOpen = document.querySelector('#galleryLogoModal');
var modalContent = document.querySelector('.modal-body');

console.log('modal content');
document.addEventListener('click', function () {
    console.log("before", modalOpen.classList)
    if (modalOpen.classList.contains('show')) {
        console.log("contains")
        modalOpen.classList.remove('show');
        modalOpen.style.cssText = "display: none !important;";
    } else {
        modalOpen.classList.add('show');
        modalOpen.style.cssText = "display: block !important;";
        console.log(modalOpen)
        
    }
    console.log("after", modalOpen.classList)
});*/




function initGallery(gallery_id, images) {
    let gallery = document.getElementById(gallery_id);

    // We want to create one image element inside the gallery for each image provide by the user
    if (gallery) {
        for (let image of images) {
            //let container = document.createElement("div");
            // <a></a>

            let div = document.createElement("div"); // <div>
            div.classList.add("thumbnail");

            let a = document.createElement("a"); // <a>
            a.setAttribute("href", image.href);
            a.setAttribute("data-lightbox", image.data);
            a.setAttribute("data-title", image.legend);

            let img = document.createElement("img"); // <img>
            img.setAttribute("src", image.href);
            img.setAttribute("data-lightbox", image.data);
            img.setAttribute("alt", image.legend);

            img.setAttribute("width", "300px")
            img.classList.add('imgModal');

            // let p = document.createElement("p");
            // p.textContent = slides[index].legend;


            //container.appendChild(img_html);
            div.appendChild(img);
            a.appendChild(div);
            gallery.appendChild(a);
            gallery.onclick = (event) => {
                // We check if we clicked on the gallery but not an image within it
                if (event.target !== event.currentTarget) return

                let gallery_root_id = '#' + gallery.parentElement.parentElement.parentElement.id
                $(gallery_root_id).modal('hide')

            }

            // MAKE PAGE STATIC WHEN MODAL IS OPENED 
            var body = document.body,
            html = document.querySelector('html');
        var buttonModal = document.getElementsByClassName('button-modal');


        for (var i = 0; i < buttonModal.length; i++) {
            buttonModal[i].addEventListener('click', function () {
                if (!body.classList.contains("modal-open")) {
                    html.classList.add("overflow-hidden");
                } else {
                    html.classList.remove("overflow-hidden");
                }
            });
        }

        var body = document.body,
            html = document.querySelector('html');

        var modal1 = document.querySelector('#galleryInteModal');
        var modal2 = document.querySelector('#galleryDigitartModal');
        var modal3 = document.querySelector('#galleryPrintModal');
        var modal4 = document.querySelector('#galleryLogoModal');


        modal1.addEventListener('click', function () {
            if (modal1 && body.classList.contains("modal-open")) {
                html.classList.remove("overflow-hidden");


            }
        });

        modal2.addEventListener('click', function () {
            if (modal2 && body.classList.contains("modal-open")) {
                html.classList.remove("overflow-hidden");


            }
        });

        modal3.addEventListener('click', function () {
            if (modal3 && body.classList.contains("modal-open")) {
                html.classList.remove("overflow-hidden");


            }
        });

        modal4.addEventListener('click', function () {
            if (modal4 && body.classList.contains("modal-open")) {
                html.classList.remove("overflow-hidden");


            }
        });
            
            
         
            /* var modal = document.getElementsByClassName('modal-body');
             console.log("modal = " + modal.length);
             for (var i = 0; i < modal.length; i++) {
                 modal[i].addEventListener('click', function () {
                     console.log("clicked modal");
                     if (modal && body.classList.contains("modal-open")) {
                         html.classList.remove("overflow-hidden");


                     }
                 });
             }*/

        }
    }
}

//FORM VALIDATION QUESTION

function validate(form) {
    if (!valid) {
        alert('Please correct the errors in the form!');
        return false;
    } else {
        return confirm('Do you really want to submit the form?');
    }
}
var contactForm = document.getElementById("contactForm");
if (contactForm) {
    document.getElementById("contactForm").addEventListener("submit", submit_form);
}

function submit_form(event) {
    event.preventDefault();
    //GET ELEMENTS FROM THE CONTACT FORM
    let inputs = document.getElementById("contactForm").elements;

    let data = {
        firstname: inputs["firstname"].value,
        lastname: inputs["lastname"].value,
        email: inputs["email"].value,
        phone: inputs["phone"].value | "",
        message: inputs["message"].value,
    }

    let serialized_data = JSON.stringify(data);


    fetch('/contact-handler.php', {
            method: 'post',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: serialized_data
        })
        .then(response => {
            if (response.status < 400) {
                alert('Votre message a bien été envoyé !');
                resetForm();
            } else {
                alert('Un problème est survenu. Votre message n\'a pas été envoyé. Veuillez réessayer.');
            }

        });
}

function resetForm() {
    document.getElementById("contactForm").reset();
}

document.addEventListener('DOMContentLoaded', function () {
    initGallery("galleryIntegrationInte", imgInte);
    initGallery("galleryIntegrationPrint", imgPrint);
    initGallery("galleryIntegrationLogo", imgLogo);
    initGallery("galleryIntegrationDigitart", imgDigitart);

    var controller = new ScrollMagic.Controller();
    //loop
    $('.section').each(function () {
        var scene = new ScrollMagic.Scene({
                triggerElement: this,
                triggerHook: 0.9,
                reverse: false
            })
            .setClassToggle(this, 'mkFadeIn')
            .addTo(controller);
    })

});