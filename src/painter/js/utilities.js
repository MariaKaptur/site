'use strict';   // Mode strict du JavaScript

/*************************************************************************************************/
/* *********************************** FONCTIONS UTILITAIRES *********************************** */
/*************************************************************************************************/
console.log('utilities here!')
function getRandom(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function display(result) {
  console.log(result);
}

function getRandomColor(){

	var r;
	var g;
	var b;
	var opacity;

	r = getRandom(0, 255);
	g = getRandom(0, 255);
	b = getRandom(0, 255);
	opacity = Math.random();
	return 'rgba(' + r + ',' + g + ','  + b + ',' + opacity + ')';
}

function getRandomColorRGB(){

	var r;
	var g;
	var b;

	r = getRandom(0, 255);
	g = getRandom(0, 255);
	b = getRandom(0, 255);
	
	return 'rgb(' + r + ',' + g + ','  + b + ')';
}

