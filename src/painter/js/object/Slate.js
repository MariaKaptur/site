'use strict';

var Slate = function (pen)
{
	this.canvas = document.getElementById('slate');
	this.context = this.canvas.getContext('2d');
    this.pen = pen;
    this.isDrawing = false;
  
    this.canvas.addEventListener('mousemove', this.onMouseMove.bind(this));
    this.canvas.addEventListener('mousedown', this.onMouseDown.bind(this));
    this.canvas.addEventListener('mouseup', this.onMouseUp.bind(this));
    this.canvas.addEventListener('mouseout', this.onMouseOut.bind(this));
    
    
    //Gestionnaires d'événements
};

// Slate.prototype.onClick = function (){
// 	console.log(this.pen.color);
// };	
Slate.prototype.onMouseDown = function (event){
	this.currentLocation = this.getMousePosition(event);
	this.context.beginPath();
	this.context.lineCap = "round";
	this.context.lineJoin = "round";
	this.context.lineWidth = this.pen.size;
	this.context.strokeStyle = this.pen.color;
	this.context.moveTo(this.currentLocation.x, this.currentLocation.y);
	this.isDrawing = true;
	// this.pen.configure(this.context);

	console.log('mouseclick')
};

Slate.prototype.onMouseMove = function (event){
	console.log('mousemove');
	var currentLocation;
	
	if(this.isDrawing){
	currentLocation = this.getMousePosition(event);    
    this.context.lineTo(currentLocation.x, currentLocation.y);
    this.context.stroke(); 
    this.currentLocation = currentLocation;
	}
};

Slate.prototype.onMouseOut = function (event){
	this.isDrawing = false;
};

Slate.prototype.onMouseUp = function (event){
	this.isDrawing = false;
	this.context.closePath();
};


Slate.prototype.getMousePosition = function (event){
	var location;
	var offset;
	var styles;

	offset = this.canvas.getBoundingClientRect();
	styles = window.getComputedStyle(this.canvas);
	return {
		x: event.clientX - offset.left - parseInt(styles.borderLeftWidth),
		y: event.clientY - offset.top - parseInt(styles.borderTopWidth)
	}
	
};

Slate.prototype.clear = function(){
	this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
};






