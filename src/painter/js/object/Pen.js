'use strict';

var Pen = function ()
{
    this.color = 'black';
    this.size = 1;
};

// Pen.prototype.configure = function(){
// 	context.lineCap = "round";
// 	context.lineJoin = "round";
// 	context.lineWidth = this.pen.size;
// 	context.strokeStyle = this.pen.color;
// }
Pen.prototype.setSize = function(size) {
	this.size = size;
};

Pen.prototype.setColor = function(color) {
	this.color = color;
};