'use strict';
console.log("Palette loaded")

var ColorPalette = function ()
{
	this.canvas = document.getElementById('colorpalette');
	this.context = this.canvas.getContext('2d');
	this.pickedcolor = {red:0, green:0, blue:0};
	this.build();
	this.canvas.addEventListener('click', this.onMouseColorPick.bind(this));
};

ColorPalette.prototype.build = function (){
	var gradientRVB = this.context.createLinearGradient(0, 0, this.canvas.width, 0);
	gradientRVB.addColorStop(0, 'rgb(255,0,0)');
	gradientRVB.addColorStop(1/6,'rgb(255,255,0)');
	gradientRVB.addColorStop(2/6, 'rgb(0,255,0)');
	gradientRVB.addColorStop(0.5, 'rgb(0,255,255)');
	gradientRVB.addColorStop(4/6,'rgb(0,0,255)');
	gradientRVB.addColorStop(5/6, 'rgb(255,0,255)');
	gradientRVB.addColorStop(1, 'rgb(255,0,0)');
	this.context.fillStyle = gradientRVB;
	this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	
	var gradientNB = this.context.createLinearGradient(0, 0, 0, this.canvas.width);
	gradientNB.addColorStop(0, 'rgb(255,255,255)');
	gradientNB.addColorStop(0.5,'rgba(255,255,255,0)');
	gradientNB.addColorStop(0.5,'rgba(0,0,0,0)');
	gradientNB.addColorStop(1, 'rgb(0,0,0)');
	this.context.fillStyle = gradientNB;
	this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	}

ColorPalette.prototype.onMouseColorPick = function(event){
	var currentLocation;
	console.log("onMouseColorPick loads !")
	this.currentLocation = this.getMousePosition(event); 
	var imageData = this.context.getImageData(this.currentLocation.x,this.currentLocation.y,1,1);
	var mapPixel = imageData.data; 
	var pickedcolor;
	this.pickedcolor = 'rgb('+mapPixel[0]+','+mapPixel[1]+','+mapPixel[2]+')';
	
	// console.log(this.pickedcolor);

	var event = new Event('colorPicker');
	document.dispatchEvent(event);
};

ColorPalette.prototype.getNewColor = function() {
	return this.pickedcolor;
};

ColorPalette.prototype.show = function(show) {
	this.canvas.classList.toggle('hide');
};


ColorPalette.prototype.getMousePosition = function (event){
	var location;
	var offset;
	var styles;

	offset = this.canvas.getBoundingClientRect();
	styles = window.getComputedStyle(this.canvas);
	return {
		x: event.clientX - offset.left - parseInt(styles.borderLeftWidth),
		y: event.clientY - offset.top - parseInt(styles.borderTopWidth)
	}
	
};


	// document.addEventListener('colorPicker', function(){

	// var event = new Event('colorPicker');
	// document.dispatchEvent(event);

