'use strict';
console.log("Program loaded")
var Program = function (){
	this.colorPalette = new ColorPalette();
	this.pen = new Pen();
	this.canvas = new Slate(this.pen);


	this.size = document.querySelector('.size');
	this.color = document.querySelector('.color');

	
};

//Ecouters d'événements de l'interface
Program.prototype.start = function (){
	console.log("Start called")
	//document.querySelector("#small").addEventListener('click', this.onClickSizeSmall.bind(this));
	//document.querySelector("#normal").addEventListener('click', this.onClickSizeNormal.bind(this));
	//document.querySelector("#big").addEventListener('click', this.onClickSizeBig.bind(this));
	document.querySelector('.size').addEventListener('click', this.onClickSizeButton.bind(this));	
	document.querySelector('.color').addEventListener('click', this.onClickColorButton.bind(this));
	document.getElementById('paletteButton').addEventListener('click', this.onClickPaletteButton.bind(this));
	document.addEventListener('colorPicker', this.setNewColor.bind(this));
	document.getElementById('eraser').addEventListener('click', this.onClickEraserButton.bind(this));
};

Program.prototype.onClickEraserButton = function(event) {
	this.canvas.clear();
}

Program.prototype.onClickSizeButton = function(event) {
	var buttonSize = event.target;
	this.pen.setSize(buttonSize.dataset.size);
}

Program.prototype.onClickColorButton = function(event) {
	var buttonColor = event.target;
	this.pen.setColor(buttonColor.dataset.color);
}

Program.prototype.onClickPaletteButton = function(event) {
	this.colorPalette.show();
}

Program.prototype.setNewColor = function(event) {
	this.pen.setColor(this.colorPalette.getNewColor());
	console.log(this.colorPalette.getNewColor());
}

/*
Program.prototype.onClickSizeSmall = function (event){
	console.log(event)
	var small = document.querySelector('#small');
	this.pen.setSize(small.dataset.size)
	console.log(small);
};

Program.prototype.onClickSizeNormal = function (event){
	var normal = document.querySelector('#normal');
	this.pen.setSize(normal.dataset.size);
	console.log(normal);
};

Program.prototype.onClickSizeBig = function (event){
	var big = document.querySelector('#big');
	this.pen.setSize(big.dataset.size)
	console.log(big);
};
*/
